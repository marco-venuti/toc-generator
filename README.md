# TOC generator
Sometimes people don't put a table of contents in their $`\LaTeX`$ documents. This script attempts to generate a table of contents from the compiled pdf.

Extracting and matching text from pdfs is tricky, so it may not work as expected.
The script outputs a minimal `.tex` file with the table of contents. This needs to be manually reviewed and corrected. The scripts also generates hyperlinks in the toc to the corresponding page, as well as bookmarks.

Once the toc is working, one can embed the original pdf for example with
```
\usepackage{pdfpages}
...
\begin{document}

\frontmatter

... potentially a title page ...

\tableofcontents

... generated toc ...

\mainmatter

\includepdf[pages=-]{path.pdf}

\end{document}
```
