#!/usr/bin/env python3

from pypdf import PdfReader
import re
from packaging import version
from sys import argv

pdf = argv[1]

reader = PdfReader(pdf)
pages = reader.pages


chapters = []
sections = []
subsections = []

# tuples like
# (10, '1.3.1', 'section name', 'section type')

for i, page in enumerate(pages):
    text = page.extract_text()

    chapters_here = re.findall(r'^Chapter ([0-9]+)\n^(.*)$', text, flags=re.MULTILINE)
    sections_here = re.findall(r'^([0-9]+\.[0-9]+) +(.*)', text, flags=re.MULTILINE)
    subsections_here = re.findall(r'^([0-9]+\.[0-9]+\.[0-9]+) +(.*)', text, flags=re.MULTILINE)

    to_append = [(i, *s, 'chapter') for s in chapters_here]
    chapters += to_append

    to_append = [(i, *s, 'section') for s in sections_here]
    sections += to_append

    to_append = [(i, *s, 'subsection') for s in subsections_here]
    subsections += to_append


# print(chapters)
# print(sections)
# print(subsections)

def pprint_toc(all_sec):
    all_sec = sorted(all_sec)
    for i in all_sec:
        print(f'{i[1]})\t{i[2]}\t\t\t{i[0]}')


levels = {
    'chapter': 0,
    'section': 1,
    'subsection': 2
}


def print_tex(all_sec):
    all_sec = sorted(all_sec, key=lambda x: version.parse(x[1]))
    with open('out.tex', 'w') as out:
        out.write(r'''
\documentclass{book}
\usepackage[bookmarks, colorlinks, linkcolor=blue]{hyperref}
\usepackage{bookmark}

\begin{document}
''')
        for sec in all_sec:
            # (10, '1.3.1', 'section name', 'section type')
            out.write(f'\\contentsline{{{sec[3]}}}{{\\numberline{{{sec[1]}}} {sec[2]}}}{{{sec[0]+1}}}{{page.{sec[0]+1}}}\n')
            out.write(f'\\bookmark[level={levels[sec[3]]}, dest=page.{sec[0]+1}]{{{sec[2]}}}\n')
        out.write(r'\end{document}')


pprint_toc(chapters+sections+subsections)
print_tex(chapters+sections+subsections)
